// Date variables

var currentTime = new Date();
var year = currentTime.getFullYear();
var month = currentTime.getMonth();
var day = currentTime.getDate();
var weekDay = currentTime.getDay();
var hour = currentTime.getHours();
var minute = currentTime.getMinutes();
var second = currentTime.getSeconds();
var sysTime = weekDay + " " + month + " " + day + " " + year + " " + hour + ":" + minute + ":" + second;

// Parse JSON with jQuery
// Creates feed sections out of JSON objects

$.ajax({
	type : "GET",
	url : "http://query.yahooapis.com/v1/public/yql?q=SELECT%20title%2C%20description%2C%20link%2C%20pubDate%0AFROM%20feed(0%2C4)%0AWHERE%20url%3D'http%3A%2F%2Ffeeds.feedburner.com%2FffAggregator'&format=json&diagnostics=true&callback=friendFeed",
	data : {
		get_param : 'value'
	},
	jsonpCallback : 'friendFeed',
	dataType : "jsonp",
	success : function(data) {"use strict";

		var obj = data.query.results.entry, section = $('<section id=\"feedData\"></section>');
		for (var i = 0, l = obj.length; i < l; ++i) {
			var feedLink = obj[i].link.href;
			var feedTitle = obj[i].title.content;

			section.append('<article class="threecol"><span><a target="_blank" href="' + feedLink + '">' + feedTitle + '</article>');
		}

		$("#footerFF").append(section);
		$('#feedData article:nth-child(3)').addClass('last');
	}
}); 