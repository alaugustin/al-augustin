function setLocalStorage() {"use strict";
	localStorage.setItem('weekDay', weekDay);
	localStorage.setItem('month', month);
	localStorage.setItem('day', day);
	localStorage.setItem('year', year);
	localStorage.setItem('sysTime2', currentTime);
}

function setSectionAttributes() {"use strict";
	$('header:first').attr("id", "mainHead");
	$('article header').addClass("articleHead");
	$("div.container").attr("id", "contentBody");
	$('article header').addClass("articleHead");
}

$(document).ready(function() {"use strict";
	setLocalStorage();
	setSectionAttributes();
	$('#footerData').append("&copy; " + year + "&nbsp;<?=$footer ?>");	
});
