<?php
/**
 *
 */
class Blog extends CI_Controller {

	function _construct() {
		parent::_construct();
	}

	function index() {

		$data['title'] = "alaugust.in | HTML(5), CSS(3), JavaScript, Client Side Web Development";
		$data['heading'] = "Al Augustin";
		$data['subHeading'] = "Why is this site looking like this? &hellip;";
		$data['mainContent'] = "<!--<p>Yeah, I can hear you now, “This guy is a UI dev and his site is looking like amateur hour”. True enough, however, having my site look like this is my way of giving myself the proverbial kick in the butt in order to “step up my game” so to speak.</p>-->
		
								<p>As much as I love the <a href=\"http://www.joomla.org/\" target=\"_blank\">Joomla!</a> CMS,  I wanted to get into something that was a little more customizable so I decided to give <a href=\"http://codeigniter.com/\" target=\"_blank\">CodeIgniter</a> a chance.</p>
						
								<p>The other technologies that I have planned for this site are <a href=\"http://www.w3schools.com/html/html5_intro.asp\" target=\"_blank\">HTML5</a>, <a href=\"http://www.w3schools.com/css3/default.asp\" target=\"_blank\">CSS3</a>, <a href=\"http://www.w3schools.com/js/default.asp\" target=\"_blank\">Javascript</a> and <a href=\"http://www.w3schools.com/ajax/default.asp\" target=\"_blank\">AJAX</a>.</p>
						
								<p>Stop by every now and then to see what changes the construction over here will bring. If you would like to see something that looks more like a site you can check out <a href=\"http://alaugustin.com/\" target=\"_blank\">alaugustin.com</a></p>";
		$data['footHead'] = "Links from the web";
		$data['footer'] = "Al Augustin <br />| Front End / UI Developer";
		//$data['todo'] = array('clean house', 'eat lunch', 'call mom');

		$this -> load -> view('blog_view', $data);
	}

}
