<!doctype html>
<html lang="en" class="no-js">
<head>
	<meta charset="utf-8">
	<title><?=$title ?></title>
	<meta name="description" content="Just stuff">
	<meta name="author" content="Al Augustin">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	
	<!-- YAHOO RESET -->
	<link rel="stylesheet" type="text/css" href="http://yui.yahooapis.com/3.6.0/build/cssreset/cssreset-min.css">
	<!-- BLUEPRINT CSS -->
	<link rel="stylesheet" type="text/css" href="http://www.blueprintcss.org/blueprint/src/typography.css">	
	<!-- 1140px Grid styles for IE -->
	<!--[if lte IE 9]><link rel="stylesheet" href="css/ie.css" type="text/css" media="screen" /><![endif]-->
	<!-- The 1140px Grid - http://cssgrid.net/ -->
	<link rel="stylesheet" href="css/1140.css" type="text/css" media="screen" />	
	<!-- CUSTOM STYLE -->
	<link rel="stylesheet" href="css/styles.css?v=1.0">
	
	<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<script src="http://modernizr.com/downloads/modernizr.js"></script>
</head>
<body>
	<header class="container">
		<div class="row">
			<div id="errorHandler" class="twelvecol last">
				<noscript>Your browser does not support JavaScript!</noscript>
			</div>
			<h1 id="main" class="twelvecol last kingthingsFont"><?=$heading ?></h1>
		</div>
	</header>
	
	<div class="container">
		<div class="row">
			<section class="ninecol">
				<article>
					<header><h2><?=$subHeading ?></h2></header>
					<?=$mainContent ?>
				</article>
			</section>
			<aside class="threecol last">
				<img src="img/yup.jpg" />
			</aside>
		</div>
	</div>
	
	<footer class="container">
		<div class="row">
			<div id="footerFF">
				<header>
					<h3 id="footLinks" class="kingthingsFont"><?=$footHead ?></h3>
				</header>
			</div>
			
			<div class="threecol">
				<span id="footerData"></span>
			</div>
			
			<div class="ninecol last">
				<iframe src="http://www.facebook.com/plugins/like.php?href=http://www.alaugust.in/"
		        scrolling="no" frameborder="0"
		        style="border:none; width:450px; height:80px"></iframe>
			</div>
		</div>
	</footer>
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js" type="text/javascript"></script>
	<script src="js/plugin.js"></script>
	<script src="js/script.js"></script>
</body>
</html>
